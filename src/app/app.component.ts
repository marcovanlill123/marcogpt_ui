import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class AppComponent {
  title = 'GPT_front_end';
  Users: any;
  question : any;
  loading : boolean = false;
  textareaValue = '';
  form = new FormGroup({
  question : new FormControl(''),
  })
  //START BY adding the constructor and ngOnInit to file
  constructor(private http: HttpClient){
  }
  checkHealth = () => {
    this.http.get('http://localhost:8090/health')
    .subscribe(data => {
      console.log(data);
    });
  }
  
  askQuestion = (payload:any) => {
    this.http.post('http://localhost:8090/question', payload)
      .subscribe(data => {
        this.loading = false;
        this.textareaValue = JSON.stringify(data);
      });
  }
  
  ngOnInit() {
    this.checkHealth()
  }

  SubmitQuestion(val:string){
    this.loading = true;
    this.Users = {...this.form.value};
    this.Users.question = this.Users.question
    let question1 = this.Users.question
    const payload = {
      question: val
    };
    this.askQuestion(payload)
  }
  

}
